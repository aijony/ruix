# Ruix

Guix channel with WIP packages or features. Everything here should eventually be upstreamed to an appropriate and well-maintained channel.

# Installation

With Guix installed add the following to ~/.config/guix/channels.scm:

```scheme
(cons* (channel
        (name 'ruix)
        (url "https://gitlab.com/aijony/ruix.git"))
       %default-channels)
```

Then run `guix pull`.